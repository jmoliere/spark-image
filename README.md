# README #



### What is this repository for? ###

* Quick summary
Docker environment required for infrastructure hosting development with the stack:
-  Kafka
- Spark
- Scala Play
- Scala actors
- Scala Streaming
- Cassandra 


### How do I get set up? ###

* Summary of set up
Pre-requisites :
- docker
- docker-compose
- docker-machine for windows/mac setup

* Deployment instructions
docker-compose up -d spark-infra
